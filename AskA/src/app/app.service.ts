import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject , Observable} from 'rxjs';
import { HttpClient, HttpHeaders, HttpEventType, HttpRequest, HttpErrorResponse, HttpEvent } from '@angular/common/http'
import { catchError, map, tap } from 'rxjs/operators';
interface Iauth {
    _id :string,
    name:String,
    email:String,
    password:String,
    phone : String,
    type : String,
    qualification : String,
    experience : String,
    resume :String,
    fileName : String
}   

interface Iname{
    contentType : string ;
    Value : string;
  }

@Injectable({
  providedIn: 'root'
})
export class AppService {
    public user :Iauth[] ;
    public logEvent :Subject<boolean> = new Subject<boolean>();
    public errorEvent :Subject<boolean> = new Subject<boolean>();
    private httpOptions_post = {
        headers: new HttpHeaders().set('Content-Type', 'multipart/form-data'),
    };
    private httpOptions_get={
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
    private httpOptions_false={
        headers: new HttpHeaders({ 'Content-Type': undefined }),
    };

    private URI = "http://192.168.0.232:8090/api/AskA/";

    constructor(private router:Router , private http : HttpClient ) { }

    check( val ){
        this.http.post<Iauth[]>(this.URI+"login" ,val , this.httpOptions_get)
        .subscribe( data => {
            if(data.length != 0){
                this.user = data;
                this.logEvent.next(true);
                sessionStorage.setItem("user",data[0]._id+"-"+data[0].name);
                this.logEvent.next(true);
                if(data[0].type == "user"){
                    this.router.navigate(["user"]);
                }else{
                    if(data[0].type == "admin"){
                        this.router.navigate(["admin"]);
                    }else{
                        this.router.navigate(["company"]);
                    }
                }
            }else{
                this.errorEvent.next(false);
            }
        });
        return false;
    }
    isLoggedIn():boolean{
        const useremail = sessionStorage.getItem("user");
        return useremail ? true : false;
    }
    logout()
    {
        this.logEvent.next(false);
        sessionStorage.clear();
    }
    signUpService(data){
        this.http.post(this.URI+"user/signup", data, this.httpOptions_get)
            .subscribe(user => {
                this.router.navigate(["login"]);
            });
    }

    addJobs(datas){
        this.http.post(this.URI+'post/job', datas , this.httpOptions_get)
        .subscribe(data =>{
            this.router.navigate(["admin"]);
        });
    }

    getJobs(data){
        return this.http.get <Object[]> (this.URI+'get/jobs/allrecords/'+data);
    }

    getJobsByCompany(compayid){
        return this.http.get <Object[]> (this.URI+'get/company/'+compayid);
        
    }

    getCompanyandUser(data):Observable<Object[]>{
        return this.http.get <Object[]> (this.URI+'getAllCompanyandUser/'+data);
    }

    deleteJobsById(id) {
        return this.http.delete<Object[]> (this.URI+'delete/job/'+id  , this.httpOptions_get);
    }

    getJobsById(id):Observable<Object[]>{
        return this.http.get <Object[]> (this.URI+'get/job/'+id);
    }

    getcurrentUser(){
        return this.user;
    }
 
    searchJobs(term: string): Observable<Object[]> {
        return this.http.get<Object[]>(this.URI+'get/records/'+term);
      }
    
    getResume(resume){
        this.http.post(this.URI+"uploads/resume", resume, this.httpOptions_get)
        .subscribe(resumes => {
            let data:object = {
                "userId" : this.getIdCurrentUser(),
                "fileId" : resumes[0]._id
            };
            this.http.post(this.URI+"update/userResume" ,data, this.httpOptions_get).subscribe(user =>{
                this.router.navigate(["user"]);
            });
        });
    }
    getIdCurrentUser(){
        return sessionStorage.getItem('user').split('-')[0];
    }

    applyJob(data){
       return this.http.post(this.URI+"apply/jobByUser", data, this.httpOptions_get);
    }

    viewAppliedJobs(data){
       return this.http.post(this.URI+"get/applied/jobsByUser", data ,  this.httpOptions_get);
    }

    getUsers(data)
    {
       return this.http.get(this.URI+"get/userandcompanyByID/"+data ,  this.httpOptions_get);
    }

    downloadResume(filedata) : Observable<Object[]>{
       return this.http.get <Object[]> (this.URI+"downloads/resume/"+filedata ,  this.httpOptions_get);
    }

    getApplyId(applyid){
       return this.http.get(this.URI+"get/applied/jobsByID/"+applyid ,  this.httpOptions_get);
       
    }

    approveJobs(applyid)
    {
       return this.http.put(this.URI+"put/Aprroved/User/"+applyid ,  this.httpOptions_get);
    }

    rejectJob(applyid){
       return this.http.delete(this.URI+"delete/application/"+applyid ,  this.httpOptions_get);
    }
}