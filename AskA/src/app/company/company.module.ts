import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { CompanyDashboardComponent } from './company-dashboard/company-dashboard.component';
import { AddJobsComponent } from './add-jobs/add-jobs.component'
import {  AppRoutingModule }  from '../app-routing.module';
import { ViewJobsComponent } from './view-jobs/view-jobs.component'

@NgModule({
    imports: [
        CommonModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
    declarations: [
        CompanyDashboardComponent, 
        AddJobsComponent, ViewJobsComponent
    ],
    exports:[
        CompanyDashboardComponent,
        AddJobsComponent
    ]
})
export class CompanyModule { }
