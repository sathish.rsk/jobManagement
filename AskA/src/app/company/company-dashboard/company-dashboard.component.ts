import { Component, OnInit } from '@angular/core';
import {  AppService }  from  '../../app.service';
import { Observable } from '../../../../node_modules/rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-company-dashboard',
  templateUrl: './company-dashboard.component.html',
  styleUrls: ['./company-dashboard.component.css']
})
export class CompanyDashboardComponent implements OnInit {

    constructor(private service : AppService , private route : Router) { }
    currentUser ;
    jobs$:Observable<Object[]>;
    ngOnInit() {
        this.currentUser = this.service.getcurrentUser();
        const compayid=sessionStorage.getItem('user').split('-')[0];
        this.jobs$= this.service.getJobsByCompany(compayid);
        
    }
    
    deleteJob(id){
        if(confirm("Do you like to delete : ")){
            this.service.deleteJobsById(id).subscribe(data => {
                this.route.navigate(["/company"]);
                const compayid=sessionStorage.getItem('user').split('-')[0];
                this.jobs$= this.service.getJobsByCompany(compayid);
            });
        }
    }
}
