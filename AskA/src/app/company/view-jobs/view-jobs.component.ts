import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from '../../app.service';
@Component({
    selector: 'app-view-jobs',
    templateUrl: './view-jobs.component.html',
    styleUrls: ['./view-jobs.component.css']
})
export class ViewJobsComponent implements OnInit {
    proddetail:Object;
    constructor( private route:ActivatedRoute, private service:AppService ) { }
    ngOnInit() {
        const id=this.route.snapshot.paramMap.get('jobid');
        this.service.getJobsById(id).subscribe((pro: Object)=>{
            this.proddetail=pro;
        });
    }

}
