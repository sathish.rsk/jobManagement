import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators ,FormBuilder , NgForm, PatternValidator} from '@angular/forms';
import { HttpClient , HttpHeaders }  from '@angular/common/http'
import { AppService } from '../../app.service';

@Component({
  selector: 'app-add-jobs',
  templateUrl: './add-jobs.component.html',
  styleUrls: ['./add-jobs.component.css']
})
export class AddJobsComponent implements OnInit {
    
    constructor(
        private fb: FormBuilder , 
        private http : HttpClient , 
        private service :AppService
    ) { }
    myForm:FormGroup ;
    isLoaded : boolean = false;
    ngOnInit() {
        this.myForm = new FormGroup({
            jTitle : new FormControl('',[Validators.required , Validators.pattern('^[a-zA-Z_-]{5,15}$')]),
            jDescription : new FormControl('',[Validators.required , Validators.pattern('^[a-zA-Z_-]{5,25}$')]),
            jMin : new FormControl('',[Validators.required , Validators.pattern('^[0-9]{1,3}$')]),
            jMax : new FormControl('',[Validators.required , Validators.pattern('^[0-9]{1,3}$')]),
            jMinW : new FormControl('',[Validators.required , Validators.pattern('^[0-9]{1,2}$')]),
            jMaxW : new FormControl('',[Validators.required , Validators.pattern('^[0-9]{1,2}$')]),
            jVacancy : new FormControl('',[Validators.required , Validators.pattern('^[0-9]{1,2}$')]),
            jLocation : new FormControl('',Validators.required),
            jIndustry : new FormControl('',Validators.required),
            jQualification :new FormControl('',Validators.required),
            jCompany : new FormControl('',Validators.required),
            jDate : new FormControl('hello')
        });
    }
    fromGroupSubmit(){
        this.myForm.value.jCompany = sessionStorage.getItem('user').split('-')[0];
        this.myForm.value.jDate = new Date();
       this.service.addJobs(this.myForm.value);
    }
    
}
