import { Component } from '@angular/core';
import  { AppService }  from  './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  logView:boolean = true;
  constructor( private serv : AppService){

  }
  ngOnInit(){
    this.logView  = this.serv.isLoggedIn();
    this.serv.logEvent.subscribe(data =>{
			this.logView = data;
		})
  }
  logout(){
    this.serv.logout();
  }
}
