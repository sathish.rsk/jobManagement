import { Component, OnInit } from '@angular/core';
import {  AppService }  from  '../../app.service';
import { Observable } from '../../../../node_modules/rxjs';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import {  debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
  
})
export class AdminDashboardComponent implements OnInit {

  public showgrid:Subject<boolean>=new Subject<boolean>();
  constructor(private service:AppService, private route : Router ) { }
  jobs:Object[];
  private searchTerms = new Subject<Object>();
  display:boolean=true;
  p: number = 1;
  ngOnInit(  ) {
     this.service.getJobs("jTitle").subscribe(data => {
      this.jobs=data;
      });
    
    this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),
  
      // ignore new term if same as previous term
      distinctUntilChanged(),
  
      // switch to new search observable each time the term changes
      switchMap((term: string) => this.service.searchJobs(term)),
    ).subscribe(data => {
      this.jobs=data;
    });

    this.showgrid.subscribe(grid => {
      this.display=grid;
    });
    }

    search(term: string): void {
      if(term == null || term == ""){
        term = '([^\s]+)';
      }
      this.searchTerms.next(term);
    }

    sort(data){
      this.service.getJobs(data).subscribe(result => {
        this.jobs=result;
      });
    }

    showData(data){
      this.display = data;
    }

    deleteJob(id){
      if(confirm("Do you like to delete : ")){
          this.service.deleteJobsById(id).subscribe(data => {
              this.route.navigate(["/admin"]);
              this.service.getJobs("jTitle").subscribe(data => {
                this.jobs=data;
                });
          });
      }
    }
}
