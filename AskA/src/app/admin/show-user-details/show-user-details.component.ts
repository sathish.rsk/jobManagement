import { Component, OnInit } from '@angular/core';
import {  AppService }  from  '../../app.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Location }  from '@angular/common';

@Component({
    selector: 'app-show-user-details',
    templateUrl: './show-user-details.component.html',
    styleUrls: ['./show-user-details.component.css']
})
export class ShowUserDetailsComponent implements OnInit {

    constructor(
        private service:AppService ,
        private route:ActivatedRoute , 
        private router : Router,
        private myloc : Location
    ) { }
    userdetails;
    approve;
    applyid;
    userid;
    ngOnInit() {
        this.userid=this.route.snapshot.paramMap.get('userid');
        this.applyid=this.route.snapshot.paramMap.get('applyid');

        this.service.getUsers(this.userid).subscribe(users => {
            this.userdetails=users;
        });

        this.service.getApplyId(this.applyid).subscribe(data => {
            this.approve=data;
        });

    }

    downloadResume(filedata){
        this.service.downloadResume(filedata).subscribe(data => {
        });
    }

    approveJobs(){
        this.service.approveJobs(this.applyid).subscribe(file => {
            this.myloc.back();
        });
    }

    rejectJob(){
        this.service.rejectJob(this.applyid).subscribe(file => {
            this.myloc.back();
        });
    }
}
