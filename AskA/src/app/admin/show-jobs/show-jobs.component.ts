import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from '../../app.service';
@Component({
  selector: 'app-show-jobs',
  templateUrl: './show-jobs.component.html',
  styleUrls: ['./show-jobs.component.css']
})
export class ShowJobsComponent implements OnInit {

  constructor(private route:ActivatedRoute, private service:AppService ) { }
  jobs;
  myArr = [];
  ngOnInit() {
    const id=this.route.snapshot.paramMap.get('jobid');
    let data = {
      jobId :"",
    };
    data["jobId"]=id;
    this.service.viewAppliedJobs(data).subscribe(result => {
      this.jobs=result;
    });
  }

}
