import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyProfileComponent } from './company-profile/company-profile.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';

import { AppRoutingModule } from  '../app-routing.module';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ViewCompanyJobsComponent } from './view-company-jobs/view-company-jobs.component';
import { ShowJobsComponent } from './show-jobs/show-jobs.component';
import { ShowUserDetailsComponent } from './show-user-details/show-user-details.component';
import { DownloadResumeComponent } from './download-resume/download-resume.component';
import {NgxPaginationModule} from 'ngx-pagination'; 

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    NgxPaginationModule
  ],
  declarations: [CompanyProfileComponent, AdminDashboardComponent, UserProfileComponent, ViewCompanyJobsComponent, ShowJobsComponent, ShowUserDetailsComponent, DownloadResumeComponent]
})
export class AdminModule { }
