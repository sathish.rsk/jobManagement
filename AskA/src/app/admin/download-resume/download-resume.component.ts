import { Component, OnInit } from '@angular/core';
import { AppService }  from '../../app.service';
import {  ActivatedRoute }  from '@angular/router';
import  { Observable }  from 'rxjs';
import  { DomSanitizer ,  SafeResourceUrl, SafeUrl }  from '@angular/platform-browser';
 
interface Iname{
  contentType : string ;
  Value : string;
}

@Component({
  selector: 'app-download-resume',
  templateUrl: './download-resume.component.html',
  styleUrls: ['./download-resume.component.css']
})
export class DownloadResumeComponent implements OnInit {

  constructor( private service  : AppService , private snaps : ActivatedRoute , private santy : DomSanitizer) { }
  private mydata :Iname[] = [];
  trust : SafeUrl;
  temp$ : Observable<Iname[]>;
  resume:Object;
  myfile = "" ;
  mytype = "" ;
  ngOnInit() {
    const id = this.snaps.snapshot.paramMap.get('fileid');
    this.service.downloadResume(id).subscribe(data => {
      this.resume=data;
    });
  } 
  function1(resume){
    let d = "data:"+resume.contentType+";base64,"+resume.Value;
    this.trust = this.santy.bypassSecurityTrustResourceUrl(d);
    return  this.trust;
  }
}
