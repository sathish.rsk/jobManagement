import { Component, OnInit } from '@angular/core';
import {  AppService }  from  '../../app.service';
import { Observable } from '../../../../node_modules/rxjs';
import { Router } from '@angular/router';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  constructor(private service : AppService , private route : Router) { }
  users$:Observable<Object[]>;

  ngOnInit() {
    this.users$= this.service.getCompanyandUser("user");
  }

}
