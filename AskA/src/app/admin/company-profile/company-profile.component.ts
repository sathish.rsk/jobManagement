import { Component, OnInit } from '@angular/core';
import {  AppService }  from  '../../app.service';
import { Observable } from '../../../../node_modules/rxjs';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.css']
})
export class CompanyProfileComponent implements OnInit {

  constructor(private service : AppService , private route:ActivatedRoute) { }
  companies$:Observable<Object[]>;
  proddetail:Object;
  ngOnInit() {
    this.companies$= this.service.getCompanyandUser("company");
  }
 
}
