import { Component, OnInit } from '@angular/core';
import {  AppService }  from  '../../app.service';
import { Observable } from '../../../../node_modules/rxjs';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-view-company-jobs',
  templateUrl: './view-company-jobs.component.html',
  styleUrls: ['./view-company-jobs.component.css']
})
export class ViewCompanyJobsComponent implements OnInit {

  constructor(private service : AppService , private route:ActivatedRoute) { }
  companyjobs:Object;

  ngOnInit() {
    const id=this.route.snapshot.paramMap.get('companyid');
    this.service.getJobsByCompany(id).subscribe((pro: Object)=>{
      this.companyjobs=pro;
  });
  }

}
