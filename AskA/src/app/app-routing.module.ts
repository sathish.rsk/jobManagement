import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component'
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { CompanyProfileComponent } from './admin/company-profile/company-profile.component';
import { UserProfileComponent } from './admin/user-profile/user-profile.component';
import { SignupComponent } from './auth/signup/signup.component';

import { AuthGuard  } from './auth/auth.guard';
import { OtherGaurd  } from './auth/auth.guard';

import { UserDashboardComponent } from './user/user-dashboard/user-dashboard.component';
import { ResumeComponent } from './user/resume/resume.component'
import  { CompanyDashboardComponent }  from './company/company-dashboard/company-dashboard.component';
import  { AddJobsComponent }  from  './company/add-jobs/add-jobs.component';
import  { ViewJobsComponent }  from  './company/view-jobs/view-jobs.component';
import { ViewCompanyJobsComponent } from './admin/view-company-jobs/view-company-jobs.component';
import { ShowJobsComponent } from './admin/show-jobs/show-jobs.component';
import { ApplyJobsComponent } from './user/apply-jobs/apply-jobs.component';
import { ShowUserDetailsComponent } from './admin/show-user-details/show-user-details.component';
import { DownloadResumeComponent } from  './admin/download-resume/download-resume.component';
const routes: Routes = [
  { 
    path: 'login',
    component: LoginComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'',
    redirectTo:'/login',
    pathMatch:'full'
  },
  {
    path:'company',
    component:CompanyDashboardComponent,
    canActivate:[OtherGaurd]
  },
  { 
    path: 'home', 
    component: AppComponent 
  }, 
  { 
    path: 'admin', 
    component: AdminDashboardComponent,
    canActivate:[OtherGaurd]
  },
  { 
    path: 'signup', 
    component: SignupComponent,
    canActivate : [AuthGuard]
  },
  { 
    path:'admin/companyprofile', 
    component:CompanyProfileComponent,
    canActivate:[OtherGaurd]
  },
  { 
    path:'admin/userprofile', 
    component:UserProfileComponent,
    canActivate:[OtherGaurd]
  },
  { 
    path:'user', 
    component:UserDashboardComponent,
    canActivate:[OtherGaurd]
  },
  {
    path:'user/uploadresume',
    component : ResumeComponent,
    canActivate:[OtherGaurd]
  },
  {
    path:"addjob",
    component : AddJobsComponent,
    canActivate:[OtherGaurd]
  },
  {
    path:"company/:jobid",
    component : ViewJobsComponent,
    canActivate:[OtherGaurd]
  },
  {
    path:"admin/companyprofile/:companyid",
    component : ViewCompanyJobsComponent,
    canActivate:[OtherGaurd]
  },
  {
    path:"admin/applications/:jobid",
    component : ShowJobsComponent,
    canActivate:[OtherGaurd]
  },
  {
    path:"admin/applications/:jobid/userdetails/:userid/:applyid",
    component : ShowUserDetailsComponent,
    canActivate:[OtherGaurd]
  },
  {
    path:"admin/applications/:jobid/userdetails/:userid/:applyid/downloads/:fileid",
    component : DownloadResumeComponent,
    canActivate:[OtherGaurd]
  }    
  
];	

@NgModule({
  imports: [
    // CommonModule
    RouterModule.forRoot(routes)
    
  ],
  declarations: [],
  exports:[
    RouterModule
  ]
})


export class AppRoutingModule {}
