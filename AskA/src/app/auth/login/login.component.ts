import { Component, OnInit , ViewEncapsulation , ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../auth/auth.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
    @ViewChild('errorLogin') errorLogin: any;
    @ViewChild('noInput') noInput: any;
    
    showErr:boolean = false;
    closeResult:string;

    constructor(private service:AuthService,private modalService: NgbModal) { }

    ngOnInit() {
        
    }

    onLoginSubmit(myForm : NgForm){
        if(myForm.valid){
            if(!this.service.callServices(myForm.value)){
                setTimeout( () => {
                    this.showErr = true;
                } , 1000);
            }
        }
        else{
            this.modalService.open(this.errorLogin, { centered: true });
        }
    }

}
