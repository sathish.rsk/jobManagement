import { Injectable } from '@angular/core';
import { RouterModule,Routes,Router } from '@angular/router';
import { Subject } from 'rxjs';
import { AppService } from '../app.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
    public loginChangeEvent:Subject<boolean>=new Subject<boolean>();
    constructor(private router:Router , private appService : AppService) { }

    callServices(val){
        this.appService.check(val);
    }
    signUpFunc(data){
        
        this.appService.signUpService(data );
        return false;
    }
}
