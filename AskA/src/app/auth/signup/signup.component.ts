import { Component, OnInit } from '@angular/core';
import { NgForm } from '../../../../node_modules/@angular/forms';
import { AuthService } from '../auth.service';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
    clicked = "user";
    Auth = true;

    constructor(private authService : AuthService) { }

    ngOnInit() {
    }

    sigUpForm(myForm : NgForm){
        if(myForm){
            if(!this.authService.signUpFunc(myForm.value)){
                this.Auth = false; 
            }
        }
    }
    
}
