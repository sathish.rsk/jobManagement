import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot , Router} from '@angular/router';
import { Observable } from 'rxjs';
 
import { AppService} from '../app.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(private service : AppService , private router:Router){

    }
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if(!this.service.isLoggedIn()){
            return true;
        }else{
            return false;
        }
    }
}
@Injectable({
    providedIn: 'root'
})
export class OtherGaurd implements CanActivate {
    constructor(private service : AppService , private router:Router){

    }
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if(this.service.isLoggedIn()){
            return true;
        }else{
            this.router.navigate(['login']);
            return false;
        }
    }
}