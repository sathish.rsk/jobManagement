import { Component, OnInit } from '@angular/core';
import { AppService } from '../../app.service';
@Component({
  selector: 'app-apply-jobs',
  templateUrl: './apply-jobs.component.html',
  styleUrls: ['./apply-jobs.component.css']
})
export class ApplyJobsComponent implements OnInit {

  constructor(private service:AppService) { }
  jobs;

  ngOnInit() {
    let data = {
      userId :"",
    };
    data["userId"]=this.service.getIdCurrentUser();
    this.service.viewAppliedJobs(data).subscribe(result => {
      this.jobs=result;
    });
  }
  tempfunc(val){
    return val == false ? "processing" : "approved";
  }
}
