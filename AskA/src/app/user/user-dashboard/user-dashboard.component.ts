import { Component, OnInit, ViewEncapsulation , ViewChild  } from '@angular/core';
import { Observable } from '../../../../node_modules/rxjs';
import { Router  } from '@angular/router';
import { Subject } from 'rxjs';
import {  debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { AppService } from '../../app.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

interface IjopApply{
    jobId :string,
    userId :string,
    isApproved: Boolean,
    jName : string,
    uName : String
}
@Component({
    selector: 'app-user-dashboard',
    templateUrl: './user-dashboard.component.html',
    styleUrls: ['./user-dashboard.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class UserDashboardComponent implements OnInit {
    @ViewChild('errorLogin') errorLogin: any;
    @ViewChild('success') success: any;
    @ViewChild('resume') resume: any;

    constructor(private service:AppService, private route : Router,private modalService: NgbModal ) { }

    public showgrid:Subject<boolean>=new Subject<boolean>();
    jobs:Object[];
    private searchTerms = new Subject<Object>();
    display:boolean=true;
    userdata;

    ngOnInit() {
        this.service.getJobs("jTitle").subscribe(data => {
            this.jobs=data;
            });
          
          this.searchTerms.pipe(
            // wait 300ms after each keystroke before considering the term
            debounceTime(300),
        
            // ignore new term if same as previous term
            distinctUntilChanged(),
        
            // switch to new search observable each time the term changes
            switchMap((term: string) => this.service.searchJobs(term)),
          ).subscribe(data => {
            this.jobs=data;
          });
      
          this.showgrid.subscribe(grid => {
            this.display=grid;
          });
    }

    search(term: string): void {
        this.searchTerms.next(term);
      }
  
      sort(data){
        this.service.getJobs(data).subscribe(result => {
          this.jobs=result;
        });
      }
  
      showData(data){
        this.display = data;
      }

      applyJob(id,title){
        let nameData = sessionStorage.getItem('user').split('-')[1];
        let data = {
          jobId : id ,
          uName : nameData,
          userId : this.service.getIdCurrentUser(),
          isApproved : false,
          jName : title
        };
        let userid= this.service.getIdCurrentUser();
        
        this.service.getUsers(userid).subscribe(user => {
          this.userdata=user;
          if(!(this.userdata.fileName==null||this.userdata.fileName==undefined))
          {
            this.service.applyJob(data).subscribe(result => {
              if(result=="Already Exist"){
                this.modalService.open(this.errorLogin, { centered: true });
              }
              else{
                this.modalService.open(this.success, { centered: true });
              }
            });
          }
          else{
            this.modalService.open(this.resume, { centered: true });
          }
        });
        
      }
}   
