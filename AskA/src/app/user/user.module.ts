import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,FormGroup, FormControl,ReactiveFormsModule }   from '@angular/forms';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { ApplyJobsComponent } from './apply-jobs/apply-jobs.component';
import { ResumeComponent } from './resume/resume.component';
import { AppRoutingModule } from '../app-routing.module';
import {NgxPaginationModule} from 'ngx-pagination'; 

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        AppRoutingModule,
        NgxPaginationModule
    ],
    declarations: [
        UserDashboardComponent, 
        ApplyJobsComponent, 
        ResumeComponent
    ],
    exports:[
        UserDashboardComponent,
        ApplyJobsComponent,
        ResumeComponent
    ]
})
export class UserModule { }
