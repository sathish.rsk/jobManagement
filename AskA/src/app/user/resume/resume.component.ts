import { Component, OnInit } from '@angular/core';
import { ElementRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from "@angular/forms";
import { AppService } from '../../../app/app.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { MyfileDirective } from '../../myfile.directive';


@Component({
    selector: 'app-resume',
    templateUrl: './resume.component.html',
    styleUrls: ['./resume.component.css']
})
@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule
    ]
})
export class ResumeComponent implements OnInit {

    ngOnInit() {
    }

    form: FormGroup;
    loading: boolean = false;
    private fileObj : File;
    private formData : FormData  = new FormData();

    @ViewChild('fileInput') fileInput: ElementRef;

    constructor(private fb: FormBuilder,private service:AppService) {
        this.createForm();
    }

    createForm() {
        this.form =this.fb.group({
            file:null,
            userId:String
        });
    }

    onFileChange(event) {
        let reader = new FileReader();
        if(event.target.files && event.target.files.length > 0) {
            this.fileObj = event.target.files;
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                    this.form.get('file').setValue({
                    name: file.name,
                    contentType : file.type,
                    Value: reader.result.split(',')[1]
                })
            };
        }
    }

    onSubmit() {
        this.form.value.userId = this.service.getIdCurrentUser();

        const formModel = this.form.value;//window.jsonToFormData(this.form.value);
        this.formData.append("file" , this.fileObj[0] , this.fileObj[0]["name"]);
        this.loading = true;
        this.service.getResume(formModel);
        
    }

    clearFile() {
        this.form.get('file').setValue(null);
        this.fileInput.nativeElement.value = '';
    }
}
