import { Directive , ElementRef } from '@angular/core';

@Directive({
  selector: '[appMyfile]'
})
export class MyfileDirective {

  constructor(el: ElementRef) {
    el.nativeElement.style.backgroundColor = 'yellow';
 }

}
