const express = require("express");
const bodyParser = require('body-parser');
const path = require('path');
const crypto = require('crypto');
const mongoose = require('mongoose');
const multer  = require('multer');
const GridFsStorage = require('multer-gridfs-storage');
const Grid = require('gridfs-stream');
const methodoverride = require('method-override');

const myApp = express();
//myApp.use(express.limit(100000000));

myApp.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", 'GET, PUT, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

myApp.use(bodyParser.json({limit: '10mb'}));
myApp.use(methodoverride('_method'));

myApp.set('view engine ' , 'ejs');

const MongoURI = "mongodb://localhost:27017/jobDB"

const options = {
    useNewUrlParser :true
};

mongoose.connect(MongoURI , options);
const conn = mongoose.connection;

let gfs;

conn.once('open', () => {
    
    gfs = Grid(conn.db , mongoose.mongo);
    gfs.collection('uploads');
    
});

const storage = new GridFsStorage({
  url: MongoURI,
  file: (req, file) => {
    return new Promise((resolve, reject) => {
      crypto.randomBytes(16, (err, buf) => {
        if (err) {
          return reject(err);
        }
        const filename = buf.toString('hex') + path.extname(file.originalname);
        const fileInfo = {
          filename: filename,
          bucketName: 'uploads'
        };
        resolve(fileInfo);
      });
    });
  }
});
const upload = multer({ storage });

myApp.get('/' ,(req , res) =>{
    res.send("myJob Application");
});
//Schemas for each Collection
let Schema = mongoose.Schema;
    //userSchema        
    let UserSchema = new Schema(
        {
            name:String,
            email:String,
            password:String,
            phone : String,
            type : String,
            qualification : String,
            experience : String,
            fileName : {type : String}
        },
        {
            collection : 'authUser'
        }
    );

    const authModel = mongoose.model('authuser', UserSchema);
//File  Schema
    let fileSchema = new Schema(
        {
            name : String,
            contentType : String,
            Value : String
        },
        {
            collection : 'resumes'
        }
    );

    const resumeModel = mongoose.model('resumes', fileSchema);
//Jobs Schema
    let JobSchema = new Schema(
        {
            jTitle : String,
            jDescription : String,
            jMin : Number,
            jMax : Number,
            jMinW : String,
            jMaxW : String,
            jVacancy : Number,
            jLocation : String,
            jIndustry : String,
            jQualification :String,
            jCompany : { type : String},
            jDate : Date
        },
        {
            collection : 'jobs'
        }
    );

    const jobModel = mongoose.model('jobModel', JobSchema);
//Applied jobs Schema
let applySchema = new Schema(
        {
            jobId : { type : String },
            userId : { type : String },
            jName : String,
            uName : String,
            isApproved:Boolean
        },
        {
            collection : 'applied'
        }
    );

    const applyModel = mongoose.model('applyModel', applySchema);    

//Api for data Manipulation

    myApp.post('/api/AskA/apply/jobByUser' , (req , res) => {
        applyModel.find({
            $and:[
                { userId:req.body.userId },
                { jobId:req.body.jobId }
            ]
        })
        .then(docs => {
            if(docs.length == 0){
                applyModel.insertMany(req.body , (err , docs) => {
                    if(err){
                        res.send(err.message);
                    }
                    res.send(docs);
                });
            }else{
                res.setHeader('content-type', 'application/json');
                res.json("Already Exist");
            }
        })
        .catch(err => {
            res.send(err);
        });
    });

    //HandShake function for job user()  

    myApp.post('/api/AskA/get/applied/jobsByUser' , ( req , res ) => {
        applyModel.find(req.body , ( err , docs ) => {
            if(err){
                res.send(err.message);
            }
            res.send(docs);
        });
    });
    
    myApp.get('/api/AskA/get/applied/jobsByID/:applyId' , (req, res) => {
        applyModel.findById(req.params.applyId , ( err , docs ) => {
            if(err){
                res.send(err.message);
            }
            res.send(docs);
        });
    });

    myApp.put('/api/AskA/put/Aprroved/User/:applyId' , (req , res) => {
        applyModel.findByIdAndUpdate(req.params.applyId , {isApproved : true} , (err , docs) => {
            if(err){
                res.send(err.message);
            }
            applyModel.findById(docs._id , (err , doc) => {
                if(err ){
                    res.send(err.message);
                }
                res.send(doc);
            });
        });
    });
    
    myApp.delete('/api/AskA/delete/application/:currId' , (req , res ) =>{
        applyModel.findByIdAndRemove(req.params.currId , (err , docs) => {
            if(err){
                res.send(err.message);
            }
            res.send(docs);
        });
    });

    myApp.post('/api/AskA/user/signup', (req, res)=> {
        let data = req.body;
        authModel.insertMany(data, (err , docs) => {
            if(err)
                res.send(err.message);
            else
            res.send(docs);
        });
    });

    myApp.get('/api/AskA/getAllCompanyandUser/:myType' , (req , res)=> {
        authModel.find({type: req.params.myType} , (err , docs) => {
            if(err){
                res.send(err.message);
            }
            res.send(docs);
        });
    });

    myApp.post('/api/AskA/uploads' , upload.single('file') , (req , res) => {
        console.log(req.body.file);
        res.send(req.file);
    });

    myApp.post('/api/AskA/uploads/resume', (req , res) => {
        authModel.findById(req.body.userId , (err , docs) => {
            if(err){
                res.send(err.message);
            }else{
                if(docs){
                    if(docs.filename != undefined || docs.filename != ""){
                        //console.log(docs.fileName);
                        resumeModel.findByIdAndRemove(docs.fileName , (err , doc) => {
                            if(err){
                                res.send(err.message);
                            }
                            //console.log(doc);
                        });
                    }
                }
            }
        });
        resumeModel.insertMany(req.body.file , (err , docs) => {
            if(err){
                res.send(err.message);
            }
            res.send(docs);
        });
    });

    myApp.get('/api/AskA/get/userandcompanyByID/:userId' , (req , res) => {
        authModel.findById(req.params.userId , (err , docs) => {
            if(err){
                res.send(err.message);
            }
            res.send(docs);
        });
    });

    myApp.post('/api/AskA/update/userResume' , (req , res) => {
        //console.log(req.body);
        authModel.findByIdAndUpdate(req.body.userId , { fileName : req.body.fileId } , (err , docs) => {
            if(err){
                res.send(err.message);
            }
            res.send(docs);
        })
    });

    myApp.get('/api/AskA/download/resume/:fileID' ,( req , res ) => {
        resumeModel.findOne({ "_id" : req.params.fileID } , (err , docs) => {
            if(!docs || docs.length === 0){
                res.send(err);
            }
            else{
                console.log(docs.contentType);
                res.setHeader('content-type', docs.contentType);
                res.send(new Buffer(docs.Value , "base64"));
            }
        });
    });
    myApp.get('/api/AskA/downloads/resume/:fileID' ,( req , res ) => {
        resumeModel.findOne({ "_id" : req.params.fileID } , (err , docs) => {
            if(!docs || docs.length === 0){
                res.send(err);
            }
            else{
                res.send(docs);
            }
        });
    });
    // myApp.get('/api/AskA/file/:fileID' ,( req , res ) => {
    //     gfs.files.findOne({ "filename" : req.params.fileID } , (err , docs) => {
    //         if(!docs || docs.length === 0){
    //             res.send(err);
    //         }
    //         else{
    //             //console.log(docs);
    //             const readstream = gfs.createReadStream(docs.filename);
    //             readstream.pipe(res);
    //         }
    //     });
    // });

    myApp.post('/api/AskA/login',function(req , res){
        //console.log(req.body);
        authModel.find({
            $and:[
                { email:req.body.email },
                { password:req.body.password }
            ]
        }).then(function(docs){
            res.send(docs);
        }).catch(function(err){
            res.send(err);
        });
    });

    myApp.post('/api/AskA/post/job' , (req , res)=>{
        //console.log(req.body);
        jobModel.insertMany(req.body , (err , docs) => {
            if(err){
                res.send(err.message);
            }
            res.send(docs);
        });
    });

    myApp.get('/api/AskA/get/job/:jobId' , (req , res) => {
        jobModel.findById(req.params.jobId , (err , docs) => {
            if(err){
                res.send(err.message);
            }
            res.send(docs);
        }); 
    });

    myApp.get('/api/AskA/get/jobs/allrecords/:sort' , (req , res) => {
        var sortData = {};
        sortData[req.params.sort] = 1;
        jobModel.find()
        .sort(sortData)
        .then(snap =>{
            res.send(snap);
        })
        .catch(err => {
            res.send(err.message);
        });
    });

    myApp.get('/api/AskA/get/company/:companyId' , (req , res) => {
        jobModel.find({jCompany : req.params.companyId})
                .then(docs => res.send(docs))
                .catch(err => res.send(err.message));
    });

    myApp.get('/api/AskA/get/records/:value' , (req , res) => {
        let regex1 = new RegExp(req.params.value);
        jobModel.find({$or:
            [
                {jTitle : {$regex: regex1 }},
                {jDescription : {$regex: regex1 }},
                {jMinW : {$regex: regex1 }},
                {jMaxW : {$regex: regex1 }},
                {jLocation : {$regex: regex1 }},
                {jIndustry : {$regex: regex1 }},
                {jQualification : {$regex: regex1 }}
            ]
        }
        ).then(docs => res.send(docs))
        .catch(err => res.send(err.message));
    });

    myApp.delete('/api/AskA/delete/job/:jobId', (req , res) => {
        jobModel.findByIdAndRemove(req.params.jobId ,(err , docs) => {
            if(err){
                res.send(err.message);
            }
            res.send(docs);
        });
    });

const port = 8090;

myApp.listen(port , () => console.log("server started on port   "+port));


