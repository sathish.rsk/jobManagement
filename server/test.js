let JobSchema = new Schema(
    {
        jTitle : String,
        jDescription : String,
        jMin : Number,
        jMax : Number,
        jMinW : String,
        jMaxW : String,
        jVacancy : Number,
        jLocation : String,
        jIndustry : String,
        jQualification :String,
        jCompanyId : String,
        jDate : Date
    },
    {
        collection : 'jobs'
    }
);

const jobModel = mongoose.model('jobModel', JobSchema);

myApp.post('/api/AskA/post/job', (req , res)=>{
    jobModel.insertMany(req.body,(err , docs) => {
        if(err)
            res.send(err.message);
        res.send(docs);
    });
});
myApp.get('/api/AskA/get/job/:jobId' , (req , res) => {
   jobModel.findById(req.params.jobId , (err , docs) => {
        if(err)
            res.send(err.message);
        res.send(docs);
   }); 
});
myApp.get('/api/AskA/get/job/all' , (req , res) => {
   jobModel.find()
            .then(docs => res.send(docs))
            .catch(err => res.send(err.message));
});
myApp.get('/api/AskA/get/company/:companyId' , (req , res) => {
    jobModel.find({jCompanyId : req.params.companyId})
            .then(docs => res.send(docs))
            .catch(err => res.send(err.message));
});
myApp.get('/api/AskA/get/records/:value' , (req , res) => {
    let regex1 = new RegExp(req.params.value);
//    var regEmail = new RegExp(req.body.email);
//    var regPassword = new RegExp(req.body.password);
    jobModel.find({$or:
        [
            
            {jTitle : {$regex: regex1 }},
            {jDescription : {$regex: regex1 }},
            {jMin : {$regex: regex1 }},
            {jMax : {$regex: regex1 }},
            {jMinW : {$regex: regex1 }},
            {jMaxW : {$regex: regex1 }},
            {jVacancy : {$regex: regex1 }},
            {jLocation : {$regex: regex1 }},
            {jIndustry : {$regex: regex1 }},
            {jQualification : {$regex: regex1 }},
            {jCompanyId : {$regex: regex1 }},
            {jDate : {$regex: regex1 }}
        ]
    }
    ).then(docs => res.send(docs))
    .catch(err => res.send(err.message));
});